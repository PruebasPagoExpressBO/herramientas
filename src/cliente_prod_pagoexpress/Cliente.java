package cliente_prod_pagoexpress;
/**
 * @autor: jose maria ignacio
 */
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Cliente {

    String host = "";
    int puerto = 0;
    String mensaje = "";
    String mensajeRespuesta = "";

    public void inciar() {
        DataInputStream in;
        DataOutputStream out;

        try {
            System.out.println("en try");
            Socket sc = new Socket(this.host, this.puerto);
            // y el out es para eviar pedidos al servidor segun el proceso 
            out = new DataOutputStream(sc.getOutputStream());

            System.out.println(" enviando mensaje:");
            out.writeBytes(this.getMensaje());
            System.out.println("recibiendo respuesta: ");
           
            //parte escencial para leer el socket de respuesta
            // para el cliente el in es para recivir los pedidos del servidor.
            in = new DataInputStream(sc.getInputStream());
            byte[] pedido = new byte[1000];
             for (int i = 0; i < pedido.length; i++)
			pedido[i] = (byte) ' ';
             
             int bytesRecibidos = in.read(pedido);
             String cad = new String(pedido, 0, bytesRecibidos);
            
             this.setMesajeRespeusta(cad);
             
            sc.close();
            System.out.println("cliente desconectado");

        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
            this.setMesajeRespeusta("sin resultado");
        }

    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getHost() {
        return this.host;
    }

    public void setPuerto(int puerto) {
        this.puerto = puerto;
    }

    public int getPuerto() {
        return this.puerto;
    }

    public void setMensaje(String mensasje) {
        this.mensaje = mensasje;
    }

    public String getMensaje() {
        return this.mensaje;
    }

    public void setMesajeRespeusta(String mensajeRespuesta) {
        this.mensajeRespuesta = mensajeRespuesta;
    }

    public String getMesajeRespuesta() {
        return this.mensajeRespuesta;
    }

}
